package edu.ntnu.idatt2001;

import edu.ntnu.idatt2001.Person;

public class Employee extends Person {

    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "edu.ntnu.idatt2001.Employee: " + super.toString();
    }
}
