package edu.ntnu.idatt2001;

import java.util.ArrayList;

public class HospitalClient {
    public static void main(String[] args) {

        Hospital hospital = new Hospital("Akutten");
        HospitalTestData.fillRegisterWithTestData(hospital);

        System.out.println(hospital.toString());
    }
}

