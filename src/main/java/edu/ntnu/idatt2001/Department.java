package edu.ntnu.idatt2001;

import java.util.ArrayList;
import java.util.Objects;

public class Department {

    private String departmentName;
    private ArrayList<Employee> employees;
    private ArrayList<Patient> patients;

    public Department(String departmentName) {
        this.departmentName = departmentName;
        this.employees = new ArrayList<>();
        this.patients = new ArrayList<>();
    }

    public Department(String departmentName, ArrayList<Employee> employees, ArrayList<Patient> patients) {
        this.departmentName = departmentName;
        this.employees = employees;
        this.patients = patients;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
    public ArrayList<Employee> getEmployees(){
        return employees;
    }

    public void addEmployee(Employee employee){
        if(!(employees.contains(employee))) {
            employees.add(employee);
        }else {
            throw new IllegalArgumentException("Could not add this employee");
        }
    }
    public ArrayList<Patient> getPatients(){
        return patients;
    }
    public void addPatient(Patient patient){
        if(!(patients.contains(patient))) {
            patients.add(patient);
        }else{
            throw new IllegalArgumentException("Could not add this patient");
        }
    }
    public void remove(Person person) throws RemoveException {
        if(person == null){
            throw new IllegalArgumentException();
        }
        if (person instanceof Employee && employees.contains(person)) {
            employees.remove(person);
        } else if (person instanceof Patient && patients.contains(person)) {
            patients.remove(person);
        } else {
            throw new RemoveException("This is neither a person of type patient nor employee");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(employees, that.employees) && Objects.equals(patients, that.patients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(employees, patients);
    }

    public String toString(){
        return "\nedu.ntnu.idatt2001.Department: " + departmentName +
                "\nEmployees: " + employees +
                "\nPatients: " + patients;
    }
}
