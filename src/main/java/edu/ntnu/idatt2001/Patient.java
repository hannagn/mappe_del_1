package edu.ntnu.idatt2001;

import edu.ntnu.idatt2001.Diagnosable;
import edu.ntnu.idatt2001.Person;

public class Patient extends Person implements Diagnosable {

    private String diagnosis = "";

    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
        this.diagnosis = diagnosis;
    }

    protected String getDiagnosis(){
        return diagnosis;
    }
    @Override
    public void setDiagnosis(String diagnosis){
        this.diagnosis = diagnosis;
    }
    @Override
    public String toString(){
        return "\nedu.ntnu.idatt2001.Patient: " + super.toString() +
                "\nDiagnose: " + diagnosis;
    }

}
