package edu.ntnu.idatt2001;

import edu.ntnu.idatt2001.Department;

import java.util.ArrayList;

public class Hospital {

    private final String hospitalName;
    private ArrayList<Department> departments;

    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
        this.departments = new ArrayList<>();
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public ArrayList<Department> getDepartments() {
        return departments;
    }

    public void addDepartment(Department department){
        if(!(departments.contains(department))) {
            departments.add(department);
        }else{
            throw new IllegalArgumentException("Could not add this department");
        }
    }

    public String toString(){
        return "\n\nedu.ntnu.idatt2001.Hospital: " + hospitalName +
                "\nDepartments: " + departments + "\n\n";
    }
}

