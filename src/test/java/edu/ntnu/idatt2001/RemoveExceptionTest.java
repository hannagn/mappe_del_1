package edu.ntnu.idatt2001;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class RemoveExceptionTest {

    private final Department newDepartment = new Department("Emergency");

    Employee employeeToBeRemoved = new Employee("Jane", "Doe", "");
    Employee unregisteredEmployee = new Employee("James", "Parker", "");
    Patient patientToBeRemoved = new Patient("John", "Smith", "");
    Patient unregisteredPatient = new Patient("Sarah", "South", "");

    @BeforeEach
    public void init() {
        newDepartment.addEmployee(employeeToBeRemoved);
        newDepartment.addPatient(patientToBeRemoved);
    }

    @Nested
    @DisplayName("Remove Patients")
    class testRemovePatients {
        @Test //test tries to remove patient that is not registered in this department
        public void testRemovePatient_removePatientNotInRegister_throwRemoveException(){
            assertThrows(RemoveException.class, () -> newDepartment.remove(unregisteredPatient));
        }

        @Test //test tries to remove blank object
        public void testRemovePatient_removeNonExistingPatient_throwIllegalArgumentException() throws IllegalArgumentException {
            assertThrows(IllegalArgumentException.class, () -> newDepartment.remove(null));
        }

        @Test //test tries to remove patient that is registered in this department
        public void testRemovePatient_removePatientInRegister_removesPatientFromDepartment() throws RemoveException {
            newDepartment.remove(patientToBeRemoved);
        }

        @Nested
        @DisplayName("Remove Employees")
        class testRemoveEmployees {
            @Test //test tries to remove employee that is not registered in this department
            public void testRemoveEmployee_removeEmployeeNotInRegister_throwRemoveException() {
                assertThrows(RemoveException.class, () -> newDepartment.remove(unregisteredEmployee));
            }

            @Test //test tries to remove blank object
            public void testRemoveEmployee_removeNonExistingEmployee_throwIllegalArgumentException() throws IllegalArgumentException {
                assertThrows(IllegalArgumentException.class, () -> newDepartment.remove(null));
            }

            @Test //test tries to remove employee that is registered in this department
            public void testRemoveEmployee_removeEmployeeInRegister_removesEmployeeFromDepartment() throws RemoveException {
                newDepartment.remove(employeeToBeRemoved);
            }

        }

    }
}
